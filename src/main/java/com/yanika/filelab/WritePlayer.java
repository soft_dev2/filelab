/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.yanika.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class WritePlayer {

    public static void main(String[] args) {
        Player o = new Player('0');
        Player x = new Player('x');
        o.Win();
        x.Loss();
        o.Draw();
        x.Draw();
        o.Win();
        x.Loss();
        System.out.println(o);
           System.out.println(x);
        FileOutputStream fos = null;
        try {
            File file = new File("palyers.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
              oos.writeObject(x);
            fos.close();
            oos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WritePlayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WritePlayer.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WritePlayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
